// Generated using webpack-cli https://github.com/webpack/webpack-cli

const TerserPlugin = require('terser-webpack-plugin');
const path = require('path');

const isProduction = process.env.NODE_ENV == 'production';

const config = {
  entry: {
    'slippi-time-conversion-tool': './dist/index.js',
    'slippi-time-conversion-tool.min': './dist/index.js'
  },
  output: {
    path: path.resolve(__dirname, '_bundles'),
    filename: '[name].js',
    libraryTarget: 'umd',
    library: 'slippi-combo-detector',
    umdNamedDefine: true
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js'],
    fallback: {
      util: require.resolve('util/'),
      stream: require.resolve('stream-browserify'),
      path: require.resolve('path-browserify'),
      fs: false,
      net: false,
      crypto: require.resolve('crypto-browserify'),
      ws: false,
      dgram: false
    }
  },
  devtool: 'source-map',
  optimization: {
    minimize: true,
    minimizer: [new TerserPlugin()],
    moduleIds: 'named'
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        loader: 'ts-loader',
        exclude: ['/node_modules/']
      }
    ]
  }
};

module.exports = () => {
  if (isProduction) {
    config.mode = 'production';
  } else {
    config.mode = 'development';
  }
  return config;
};
