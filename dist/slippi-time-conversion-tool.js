#! /usr/bin/env node
"use strict";
/**
 * Name
 *      slippi-time-conversion-tool - converts frames to time elapsed and
 *                                    vice-versa for a Slippi game
 *
 * Synopsis
 *      slippi-time-conversion-tool [--verision | -v] [--help | -h] <command>
 *                                  <arg> [--file=<file>]
 *
 * Description
 *      The Slippi Time Conversion Tool is a fast tool to quickly convert
 *      between frame number and time elapsed, and vice-versa for a Slippi
 *      game. The tool can be used both as a command line interface as well
 *      as an API for other Javascript or TypeScript projects.
 *
 * Options
 *      <arg>
 *          The argument to the Slippi Time Conversion Tool. The format of
 *          the argument depends on the command. If the command is 'time'
 *          then the argument must be a non-negative integer. If the command
 *          is 'frame' then the argument must match the following regex
 *          expression: [0-9]{1,2}:[0-9]{2}. This expression equates to a
 *          string representing the minutes and seconds elapsed, i.e 'mm:ss'.
 *      --version, -v
 *          Prints the Slippi Time Conversion Tool version.
 *      --help, -h
 *          Prints the synopsis and a list of the commands.
 *      --file=<file>
 *          When a file is supplied, the Slippi Time Conversion Tool will
 *          check that the resulting conversion time/frame exists within the
 *          supplied game. The file must be a valid Slippi game recording.
 *
 * Slippi Time Conversion Tool Commands
 *      There are two available commands: 'time' and 'frame'.
 *
 *      time
 *          The 'time' command signals to the Slippi Time Conversion Tool that
 *          the input argument will be converted to the standard time elapsed
 *          format of "mm:ss". Therefore, the input argument must be a single
 *          integer representing the frame number.
 *
 *      frame
 *          The 'frame' command signals to the Slippi Time Conversion Tool that
 *          the input argument will be converted to an integer representing the
 *          frame number. Therefore, the input argument must be in the time
 *          elapsed format.
 *
 * Examples
 *      Convert a time elapsed to a frane number with no Slippi game checks:
 *          $ slippi-time-conversion-tool time 1000
 *
 *      Convert a time elapsed to a frame number with Slippi game checks:
 *          $ slippi-time-conversion-tool time 1000 --file=game.slp
 *
 *      Convert a frame number to time elapsed with no Slippi game checks:
 *          $ slippi-time-conversion-tool frame 02:45
 *
 *      Convert a frame number to time elapsed with Slippi game checks:
 *          $ slippi-time-conversion-tool frame 02:24 --file=game.slp
 *
 * Interactive
 *      When no inputs are given, then the tool will enter interactive mode.
 *      Here, the user will be able to see a simple repl:
 *          stct >
 *
 *      The following commands exist and will change the behavior of the repl:
 *          help
 *              Prints the synopsis and a list of the commands.
 *          version
 *              Prints the version of the Slippi Time Conversion Tool.
 *          load
 *              Prompts the user to load a file in for Slippi game checks.
 *              The prompt will look as follows:
 *                  *** Enter path of Slippi game file to load ***
 *                  stct-loader >
 *          time
 *              Prompts the user for an integer representing a frame number
 *              to convert to the time elapsed. If a Slippi game is loaded,
 *              then the game will be used for the Slippi game checks. The
 *              prompt will look as follows:
 *                  *** Enter the frame number ***
 *                  stct-to-time >
 *          frame
 *              Prompts the user for a time elapsed input to convert to a
 *              frame number. If a Slippi game is loaded, then the game will
 *              be used for the Slippi game checks. The prompt will look
 *              as follows:
 *                  *** Enter the time elapsed (format = mm:ss) **
 *                  stct-to-frame >
 */
exports.__esModule = true;
var tslib_1 = require("tslib");
var process_1 = require("process");
var constants_json_1 = tslib_1.__importDefault(require("./constants.json"));
var repl_1 = require("./repl");
var TimeConverter_1 = require("./TimeConverter");
function cli(argv) {
    var _a, _b, _c, _d, _e, _f, _g, _h;
    // drop the first two as they are node and the file path
    var args = argv.splice(2);
    var num_args = args.length;
    // case tree
    // if num_args is 0 then enter interactive mode
    // if num_args is 1 then it could either be version or help
    // if num_args is 2 then it is a command and an input
    // if num_args is 3 then it is a command and an input with a file
    if (num_args == 0) {
        (0, repl_1.repl)();
        (0, process_1.exit)(0);
    }
    else if (num_args == 1) {
        // this is safe due to the pre-condition of num_args == 1
        var arg = (_a = args[0]) !== null && _a !== void 0 ? _a : '';
        // case on the argument
        switch (arg) {
            // version cases
            case '--version': {
                console.log("".concat(constants_json_1["default"]['cli-name'], " - version ").concat(constants_json_1["default"]['version']));
                break;
            }
            case '-v': {
                console.log("".concat(constants_json_1["default"]['cli-name'], " - version ").concat(constants_json_1["default"]['version']));
                break;
            }
            // help cases
            case '--help': {
                console.log('slippi-time-conversion-tool [--verision | -v] [--help | -h] <command> <arg> [--file=<file>]');
                console.log('Commands: time, frame');
                break;
            }
            case '-h': {
                console.log('slippi-time-conversion-tool [--verision | -v] [--help | -h] <command> <arg> [--file=<file>]');
                console.log('Commands: time, frame');
                break;
            }
            // not version or help so we go to default which is an error
            default: {
                console.log("error: unknown command input: ".concat(arg));
                break;
            }
        }
        (0, process_1.exit)(0);
    }
    else if (num_args == 2) {
        // this is safe by the pre-condition that num_args is 2
        var arg_1 = (_b = args[0]) !== null && _b !== void 0 ? _b : '';
        var arg_2 = (_c = args[1]) !== null && _c !== void 0 ? _c : '';
        // proper inputs are of the form: command input
        // therefore we case on the command and act on the input
        switch (arg_1) {
            // convert to time elapsed (mm:ss)
            case 'time': {
                var tc = new TimeConverter_1.TimeConverter(null, false);
                var time = tc.toTime(arg_2);
                console.log("Time: ".concat(time));
                break;
            }
            // convert to frame number
            case 'frame': {
                var tc = new TimeConverter_1.TimeConverter(null, false);
                var frame = tc.toFrames(arg_2);
                console.log("Frame: ".concat(frame));
                break;
            }
            // command input is not time or frame so we error out
            default: {
                console.log("error: unknown command input: ".concat(arg_1, " ").concat(arg_2));
                (0, process_1.exit)(1);
            }
        }
        (0, process_1.exit)(0);
    }
    else if (num_args == 3) {
        // this is safe due to the pre-condition that num_args is 3
        var arg_1 = (_d = args[0]) !== null && _d !== void 0 ? _d : '';
        var arg_2 = (_e = args[1]) !== null && _e !== void 0 ? _e : '';
        var arg_3 = (_f = args[2]) !== null && _f !== void 0 ? _f : '';
        // regex commands to test the first argument for being a command
        // or a file input
        var regex_command = /^time|frame$/;
        var regex_file = /^--file=([0-9a-zA-Z_/.\- ]+\.slp)$/;
        // use the regex commands to get the test information
        var arg_1_is_cmd = regex_command.test(arg_1);
        var arg_1_is_file = regex_file.test(arg_1);
        if (arg_1_is_cmd && !arg_1_is_file) {
            // command input --file=<file>
            var arg_3_file_tk = arg_3.match(regex_file);
            if (arg_3_file_tk == null) {
                console.log("error: invalid file input: ".concat(arg_3));
                (0, process_1.exit)(1);
            }
            // get the file name from the regex group
            var fname = (_g = arg_3_file_tk[1]) !== null && _g !== void 0 ? _g : null;
            var tc = new TimeConverter_1.TimeConverter(fname);
            // case on the command argument
            switch (arg_1) {
                // convert to time elapsed (mm:ss)
                case 'time': {
                    var time = tc.toTime(arg_2);
                    console.log("Time: ".concat(time));
                    break;
                }
                // convert to frame number
                case 'frame': {
                    var frame = tc.toFrames(arg_2);
                    console.log("Frame: ".concat(frame));
                    break;
                }
                // input is not time or frame so error out
                default: {
                    console.log("error: unknown command input: ".concat(arg_1, " ").concat(arg_2, " ").concat(arg_3));
                    (0, process_1.exit)(1);
                }
            }
        }
        else if (arg_1_is_file && !arg_1_is_cmd) {
            // --file=<file> command input
            var arg_1_file_tk = arg_1.match(regex_file);
            if (arg_1_file_tk == null) {
                console.log("error: invalid file input ".concat(arg_1));
                (0, process_1.exit)(1);
            }
            var fname = (_h = arg_1_file_tk[1]) !== null && _h !== void 0 ? _h : null;
            var tc = new TimeConverter_1.TimeConverter(fname);
            // case on the command input
            switch (arg_2) {
                // convert to time elapsed (mm:ss)
                case 'time': {
                    var time = tc.toTime(arg_3);
                    console.log("Time: ".concat(time));
                    break;
                }
                // convert to frame number
                case 'frame': {
                    var frame = tc.toFrames(arg_3);
                    console.log("Frame: ".concat(frame));
                    break;
                }
                // input is not time or frame so error out
                default: {
                    console.log("error: unknown command input: ".concat(arg_1, " ").concat(arg_2, " ").concat(arg_3));
                    (0, process_1.exit)(1);
                }
            }
        }
        else {
            // we should never get here...
            console.log('fatal error: unreachable code was reached');
            (0, process_1.exit)(1);
        }
        (0, process_1.exit)(0);
    }
    else {
        // too many args case
        var input = args.join(' ');
        console.log("error: too many inputs (num args = ".concat(num_args, "): ").concat(input));
        (0, process_1.exit)(1);
    }
}
cli(process.argv);
//# sourceMappingURL=slippi-time-conversion-tool.js.map