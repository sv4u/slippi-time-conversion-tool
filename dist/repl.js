"use strict";
exports.__esModule = true;
exports.repl = void 0;
var tslib_1 = require("tslib");
var prompt_sync_1 = tslib_1.__importDefault(require("prompt-sync"));
var constants_json_1 = tslib_1.__importDefault(require("./constants.json"));
var TimeConverter_1 = require("./TimeConverter");
var prompt = (0, prompt_sync_1["default"])();
function repl() {
    console.log("".concat(constants_json_1["default"]['repl-name'], " - version ").concat(constants_json_1["default"]['version']));
    var result = prompt(constants_json_1["default"]['default-repl']);
    // initialize empty time converter
    var tc = new TimeConverter_1.TimeConverter(null, false);
    var _quit = false;
    while (!_quit) {
        switch (result) {
            case 'quit': {
                _quit = true;
                console.log('shutting down :)');
                break;
            }
            case 'help': {
                console.log('Commands: help, version, quit, load, time, frame');
                console.log('\thelp: displays this help message');
                console.log('\tversion: displays the version of this tool');
                console.log('\tquit: quits the tool');
                console.log('\tload: load a Slippi file for usage');
                console.log('\ttime: convert to time elapsed');
                console.log('\tframe: convert to frames elapsed');
                result = prompt(constants_json_1["default"]['default-repl']);
                break;
            }
            case 'version': {
                console.log("version ".concat(constants_json_1["default"]['version']));
                result = prompt(constants_json_1["default"]['default-repl']);
                break;
            }
            case 'load': {
                console.log('*** Enter path of Slippi game file to load ***');
                result = prompt(constants_json_1["default"]['loader-repl']);
                // update time converter
                tc = new TimeConverter_1.TimeConverter(result, true);
                console.log("Read ".concat(result, " into ").concat(constants_json_1["default"]['repl-name']));
                result = prompt(constants_json_1["default"]['default-repl']);
                break;
            }
            case 'time': {
                console.log('*** Please enter the frame number ***');
                result = prompt(constants_json_1["default"]['time-repl']);
                // call converter
                var time = tc.toTime(result);
                console.log("Time Elapsed: ".concat(time));
                result = prompt(constants_json_1["default"]['default-repl']);
                break;
            }
            case 'frame': {
                console.log('*** Please enter the time elapsed (format = mm:ss) ***');
                result = prompt(constants_json_1["default"]['frame-repl']);
                // call converter
                var frame = tc.toFrames(result);
                console.log("Frame : ".concat(frame));
                result = prompt(constants_json_1["default"]['default-repl']);
                break;
            }
            default: {
                console.log("unknown command: ".concat(result));
                result = prompt(constants_json_1["default"]['default-repl']);
                break;
            }
        }
    }
}
exports.repl = repl;
//# sourceMappingURL=repl.js.map