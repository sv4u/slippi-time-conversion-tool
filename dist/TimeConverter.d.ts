/**
 * Time Converter
 *
 * Given a slippi game and optional input for sanity checking, convert
 * between time elapsed and frames.
 *
 * The first valid frame is frame -123. Frame 0 is when the timer starts
 * counting down, so "00:00" will map to frame 0. When the frame is negative
 * then the negative time value of the absolute value of the frame is given.
 */
import { SlippiGame } from '@slippi/slippi-js';
export declare class TimeConverter {
    game: SlippiGame | null;
    gameCheck: boolean;
    constructor(file: string | null, _gameCheck?: boolean);
    toFrames(time: string): number | undefined;
    toTime(frame: string): string | undefined;
}
