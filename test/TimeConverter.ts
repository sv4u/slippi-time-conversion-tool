import assert from 'assert';
import { TimeConverter } from '../src/TimeConverter';

// number of tests
const TESTS = 100;

// get random frame and time elapsed
function randtimeframe(): [string, number] {
  const csum: (value: number) => number = (
    (sum: number) => (value: number) =>
      (sum += value)
  )(0);

  // weights for random picking
  const items = [-1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
  const weights = [1, 1, 6, 9, 8, 5, 4, 3, 2, 1, 1];
  const c_weights = weights.map(csum);

  const max_c_weight: number = c_weights[c_weights.length - 1];
  const _r: number = max_c_weight * Math.random();
  let index = 0;
  let found = false;
  c_weights.forEach((value: number, idx: number) => {
    if (_r <= value && !found) {
      index = idx;
      found = true;
    }
  });

  // store and be ready to use
  const selected: number = items[index];
  const is_negative = selected < 0;

  // get the actual numbers
  const minute: number = is_negative ? -items[index] : items[index];
  const second: number = Math.floor(Math.random() * 60);
  const frames: number = (is_negative ? -1 : 1) * (second + minute * 60) * 60;

  return [
    (is_negative ? `-` : ``) +
      (minute < 10 ? `0${minute}:` : `${minute}:`) +
      (second < 10 ? `0${second}` : `${second}`),
    frames
  ];
}

export function tc_tests() {
  describe('Time Converter Tests', () => {
    describe('no file input', () => {
      const tc: TimeConverter = new TimeConverter(null, false);

      describe('null tests', () => {
        it(`tc != null`, () => {
          assert.notEqual(tc, null);
        });
      });

      describe('#toFrames', () => {
        const looper: number[] = Array.from(Array(TESTS).keys());
        looper.forEach((idx: number) => {
          const [r_time, r_frame]: [string, number] = randtimeframe();
          const frame: number | undefined = tc.toFrames(r_time);
          it(`test #${idx}: tc1 @ time ${r_time} elapsed = ${r_frame}`, () => {
            assert.equal(frame, r_frame);
          });
        });
      });

      describe('#toTime', () => {
        const looper: number[] = Array.from(Array(TESTS).keys());
        looper.forEach((idx: number) => {
          const [r_time, r_frame]: [string, number] = randtimeframe();
          const frame = `${r_frame}`;
          const time: string | undefined = tc.toTime(frame);
          it(`test #${idx}: tc1 @ frame ${r_frame} = ${r_time}`, () => {
            assert.equal(time, r_time);
          });
        });
      });
    });

    describe('with a file input', () => {
      const fnames: string[] = ['slippis/test.slp'];

      for (const i in fnames) {
        const fname = fnames[i];
        describe(`using file: ${fname}`, () => {
          const tc = new TimeConverter(fname, false);

          describe('null tests', () => {
            it(`tc != null`, () => {
              assert.notEqual(tc, null);
            });
          });

          describe('#toFrames', () => {
            const looper: number[] = Array.from(Array(TESTS).keys());
            looper.forEach((idx: number) => {
              const [r_time, r_frame]: [string, number] = randtimeframe();
              const frame: number | undefined = tc.toFrames(r_time);
              it(`test #${idx}: tc @ time ${r_time} elapsed = ${r_frame}`, () => {
                assert.equal(frame, r_frame);
              });
            });
          });

          describe('#toTime', () => {
            const looper: number[] = Array.from(Array(TESTS).keys());
            looper.forEach((idx: number) => {
              const [r_time, r_frame]: [string, number] = randtimeframe();
              const frame = `${r_frame}`;
              const time: string | undefined = tc.toTime(frame);
              it(`test #${idx}: tc @ frame ${r_frame} = ${r_time}`, () => {
                assert.equal(time, r_time);
              });
            });
          });
        });
      }
    });
  });
}
