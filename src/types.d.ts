import { SlippiGame } from '@slippi/slippi-js';

export interface TimeConverterClass {
  game: SlippiGame | null;
  gameCheck: boolean;

  toFrames: (time: string) => number | undefined;
  toTime: (frame: string) => string | undefined;
}
