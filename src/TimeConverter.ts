/**
 * Time Converter
 *
 * Given a slippi game and optional input for sanity checking, convert
 * between time elapsed and frames.
 *
 * The first valid frame is frame -123. Frame 0 is when the timer starts
 * counting down, so "00:00" will map to frame 0. When the frame is negative
 * then the negative time value of the absolute value of the frame is given.
 */
import { FrameEntryType, FramesType, SlippiGame } from '@slippi/slippi-js';

export class TimeConverter {
  game: SlippiGame | null;
  gameCheck: boolean;

  /* istanbul ignore next */
  constructor(file: string | null, _gameCheck = true) {
    this.gameCheck = _gameCheck ?? false;
    this.game = _gameCheck ? new SlippiGame(file ?? '') : null;
  }

  // convert time elapsed in format mm:ss to frames
  toFrames(time: string): number | undefined {
    const TIME_REGEX_EXP = '^([-]?)([0-9]{1,2}):([0-9]{2})$';
    const tokens: RegExpMatchArray | null = time.match(TIME_REGEX_EXP);

    // sanity check on input tokens
    /* istanbul ignore if */
    if (tokens == null) {
      console.log(
        `error: invalid input time (input: ${time}) passed into 'toFrames' within TimeConverter`
      );

      return undefined;
    }

    // tokens is structured as follows
    // [ <time>, <negative>, <minutes>, <seconds>, index: <starting index>, input: <time>, groups: undefined ]
    const is_negative = tokens[1] == '-';
    const minutes = parseInt(tokens[2]);
    const seconds = parseInt(tokens[3]);

    const total_seconds = seconds + minutes * 60;
    const frames = (is_negative ? -1 : 1) * total_seconds * 60;

    // check if we are doing sanity checks
    /* istanbul ignore if */
    if (this.gameCheck) {
      // yes we sanity check
      // check if the game is null
      if (this.game == null) {
        console.log(`error: supplied game is null`);

        return undefined;
      }

      // get frames
      const gameFrames: FramesType | undefined = this.game.getFrames();

      /* istanbul ignore if */
      if (gameFrames == undefined) {
        console.log(`error: supplied game has undefined frames`);

        return undefined;
      }

      // get frame in question and check that it exists
      const frameInQuestion: FrameEntryType = gameFrames[frames];

      if (frameInQuestion == null || frameInQuestion == undefined) {
        return undefined;
      }
    }

    return frames;
  }

  // convert frames to time elapsed
  toTime(frame: string): string | undefined {
    const FRAME_REGEX_EXP = '^[-]?[0-9]+$';
    const tokens: RegExpMatchArray | null = frame.match(FRAME_REGEX_EXP);

    // sanity check on input tokens
    /* istanbul ignore if */
    if (tokens == null) {
      console.log(
        `error: invalid frame (input: ${frame}) passed into 'toTime' within TimeConverter`
      );

      return undefined;
    }

    // tokens is structured as follows
    // [ <frame>, <frame>, index: <starting index>, input: <frame>, groups: undefined ]
    let n_frame = parseInt(frame);

    // check if negative and update value if needed
    const is_negative = n_frame < 0;
    if (is_negative) {
      n_frame *= -1;
    }

    // sanity check that we are operating on non-negative numbers
    if (n_frame < 0) {
      console.log(`error: frame somehow became negative after normalization`);

      return undefined;
    }

    // 60 frames/second
    const total_seconds = Math.floor(n_frame / 60);
    const minutes = Math.floor(total_seconds / 60);
    const seconds = total_seconds % 60;

    // format the time nicely
    /* istanbul ignore next */
    const time =
      (is_negative ? `-` : ``) +
      (minutes < 10 ? `0${minutes}:` : `${minutes}:`) +
      (seconds < 10 ? `0${seconds}` : `${seconds}`);

    // do we do sanity game checks
    /* istanbul ignore if */
    if (this.gameCheck) {
      // yes we do the checks
      // check and make sure we have a game to check
      if (this.game == null) {
        console.log(`error: supplied game is null`);

        return undefined;
      }

      // get the frames
      const gameFrames: FramesType | undefined = this.game.getFrames();

      /* istanbul ignore if */
      if (gameFrames == undefined) {
        console.log(`supplied game has undefined frames`);

        return undefined;
      }

      // get the frame in question and check it
      const frameInQuestion: FrameEntryType = gameFrames[n_frame];
      if (frameInQuestion == null || frameInQuestion == undefined) {
        return undefined;
      }
    }

    return time;
  }
}
