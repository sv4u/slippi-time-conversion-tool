# Slippi Time Conversion Tool

Author: Sasank 'squatch/jbub' Vishnubhatla

Pipeline Status: ![Pipeline Status](https://gitlab.com/sv4u/slippi-time-conversion-tool/badges/main/pipeline.svg)

Coverage Status: ![Coverage Status](https://gitlab.com/sv4u/slippi-time-conversion-tool/badges/main/coverage.svg)

## Installation

To use this tool, install it via `npm`: `npm install slippi-time-conversion-tool -g --save-dev`.

## Usage

### As a `npm` Package

To use the Slippi Time Conversion Tool within your project, import `TimeConverter`. Then, we you have the path of a Slippi game, you can initialize a `TimeConverter` object as follows:

```typescript
const tc: TimeConverter = new TimeConverter(slippi_fname, true);
```

The signature of a `TimeConverter` is: `TimeConverter(file: string | null, _gameCheck: boolean = true)`. The `_gameCheck: boolean` option is an optional check of the game to ensure the time being converted exists within the game. When supplying no game, then this option should be supplied as `false`.

With a `TimeConverter` object, we can convert from frame number to time elapsed and back as follows:

```typescript
// assume start_frame: number is the frame number we are converting from
const time: string | undefined = tc.toTime(start_frame); // from frame to time
const frame: number | undefined = tc.toFrame(time); // from time to frame
```

### As a Command Line Tool

To use the Slippi Time Conversion Tool as a command line tool, all we have to use is the `npx` command.

 ```bash
 slippi-time-conversion-tool [--verision | -v] [--help | -h] <command> <arg> [--file=<file>]
 ```

The Slippi Time Conversion Tool is a fast tool to quickly convert between frame number and time elapsed, and vice-versa for a Slippi
game.

#### Options

- \<arg>
  - The argument to the Slippi Time Conversion Tool. The format of the argument depends on the command. If the command is 'time' then the argument must be a non-negative integer. If the command is 'frame' then the argument must match the following regex expression: [0-9]{1,2}:[0-9]{2}. This expression equates to a string representing the minutes and seconds elapsed, i.e 'mm:ss'.

- --version, -v
  - Prints the Slippi Time Conversion Tool version.
- --help, -h
  - Prints the synopsis and a list of the commands.

- --file=\<file>
  - When a file is supplied, the Slippi Time Conversion Tool will check that the resulting conversion time/frame exists within the supplied game. The file must be a valid Slippi game recording.

#### Slippi Time Conversion Tool Commands

There are two available commands: 'time' and 'frame'.

- time
  - The 'time' command signals to the Slippi Time Conversion Tool that the input argument will be converted to the standard time elapsed format of "mm:ss". Therefore, the input argument must be a single integer representing the frame number.

- frame
  - The 'frame' command signals to the Slippi Time Conversion Tool that the input argument will be converted to an integer representing the frame number. Therefore, the input argument must be in the time elapsed format.

#### Examples

Convert a time elapsed to a frane number with no Slippi game checks:

```bash
slippi-time-conversion-tool time 1000
```

Convert a time elapsed to a frame number with Slippi game checks:

```bash
slippi-time-conversion-tool time 1000 --file=game.slp
```

Convert a frame number to time elapsed with no Slippi game checks:

```bash
slippi-time-conversion-tool frame 02:45
```

Convert a frame number to time elapsed with Slippi game checks:

```bash
slippi-time-conversion-tool frame 02:24 --file=game.slp
```

#### Interactive

When no inputs are given, then the tool will enter interactive mode.

Here, the user will be able to see a simple repl:

```bash
stct >
```

The following commands exist and will change the behavior of the repl:

- help
  - Prints the synopsis and a list of the commands.
- version
  - Prints the version of the Slippi Time Conversion Tool.
- load
  - Prompts the user to load a file in for Slippi game checks. The prompt will look as follows:

```bash
***Enter path of Slippi game file to load***
stct-loader >
```

- time
  - Prompts the user for an integer representing a frame number to convert to the time elapsed. If a Slippi game is loaded, then the game will be used for the Slippi game checks. The prompt will look as follows:

```bash
***Enter the frame number***
stct-to-time >
```

- frame
  - Prompts the user for a time elapsed input to convert to a frame number. If a Slippi game is loaded, then the game will be used for the Slippi game checks. The prompt will look as follows:

```bash
*** Enter the time elapsed (format = mm:ss) **
stct-to-frame >
```
