import PromptSync from 'prompt-sync';
import CONSTANTS from './constants.json';
import { TimeConverter } from './TimeConverter';
var prompt = PromptSync();
export function repl() {
    console.log("".concat(CONSTANTS['repl-name'], " - version ").concat(CONSTANTS['version']));
    var result = prompt(CONSTANTS['default-repl']);
    // initialize empty time converter
    var tc = new TimeConverter(null, false);
    var _quit = false;
    while (!_quit) {
        switch (result) {
            case 'quit': {
                _quit = true;
                console.log('shutting down :)');
                break;
            }
            case 'help': {
                console.log('Commands: help, version, quit, load, time, frame');
                console.log('\thelp: displays this help message');
                console.log('\tversion: displays the version of this tool');
                console.log('\tquit: quits the tool');
                console.log('\tload: load a Slippi file for usage');
                console.log('\ttime: convert to time elapsed');
                console.log('\tframe: convert to frames elapsed');
                result = prompt(CONSTANTS['default-repl']);
                break;
            }
            case 'version': {
                console.log("version ".concat(CONSTANTS['version']));
                result = prompt(CONSTANTS['default-repl']);
                break;
            }
            case 'load': {
                console.log('*** Enter path of Slippi game file to load ***');
                result = prompt(CONSTANTS['loader-repl']);
                // update time converter
                tc = new TimeConverter(result, true);
                console.log("Read ".concat(result, " into ").concat(CONSTANTS['repl-name']));
                result = prompt(CONSTANTS['default-repl']);
                break;
            }
            case 'time': {
                console.log('*** Please enter the frame number ***');
                result = prompt(CONSTANTS['time-repl']);
                // call converter
                var time = tc.toTime(result);
                console.log("Time Elapsed: ".concat(time));
                result = prompt(CONSTANTS['default-repl']);
                break;
            }
            case 'frame': {
                console.log('*** Please enter the time elapsed (format = mm:ss) ***');
                result = prompt(CONSTANTS['frame-repl']);
                // call converter
                var frame = tc.toFrames(result);
                console.log("Frame : ".concat(frame));
                result = prompt(CONSTANTS['default-repl']);
                break;
            }
            default: {
                console.log("unknown command: ".concat(result));
                result = prompt(CONSTANTS['default-repl']);
                break;
            }
        }
    }
}
//# sourceMappingURL=repl.js.map