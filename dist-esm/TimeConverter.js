/**
 * Time Converter
 *
 * Given a slippi game and optional input for sanity checking, convert
 * between time elapsed and frames.
 *
 * The first valid frame is frame -123. Frame 0 is when the timer starts
 * counting down, so "00:00" will map to frame 0. When the frame is negative
 * then the negative time value of the absolute value of the frame is given.
 */
import { SlippiGame } from '@slippi/slippi-js';
var TimeConverter = /** @class */ (function () {
    /* istanbul ignore next */
    function TimeConverter(file, _gameCheck) {
        if (_gameCheck === void 0) { _gameCheck = true; }
        this.gameCheck = _gameCheck !== null && _gameCheck !== void 0 ? _gameCheck : false;
        this.game = _gameCheck ? new SlippiGame(file !== null && file !== void 0 ? file : '') : null;
    }
    // convert time elapsed in format mm:ss to frames
    TimeConverter.prototype.toFrames = function (time) {
        var TIME_REGEX_EXP = '^([-]?)([0-9]{1,2}):([0-9]{2})$';
        var tokens = time.match(TIME_REGEX_EXP);
        // sanity check on input tokens
        /* istanbul ignore if */
        if (tokens == null) {
            console.log("error: invalid input time (input: ".concat(time, ") passed into 'toFrames' within TimeConverter"));
            return undefined;
        }
        // tokens is structured as follows
        // [ <time>, <negative>, <minutes>, <seconds>, index: <starting index>, input: <time>, groups: undefined ]
        var is_negative = tokens[1] == '-';
        var minutes = parseInt(tokens[2]);
        var seconds = parseInt(tokens[3]);
        var total_seconds = seconds + minutes * 60;
        var frames = (is_negative ? -1 : 1) * total_seconds * 60;
        // check if we are doing sanity checks
        /* istanbul ignore if */
        if (this.gameCheck) {
            // yes we sanity check
            // check if the game is null
            if (this.game == null) {
                console.log("error: supplied game is null");
                return undefined;
            }
            // get frames
            var gameFrames = this.game.getFrames();
            /* istanbul ignore if */
            if (gameFrames == undefined) {
                console.log("error: supplied game has undefined frames");
                return undefined;
            }
            // get frame in question and check that it exists
            var frameInQuestion = gameFrames[frames];
            if (frameInQuestion == null || frameInQuestion == undefined) {
                return undefined;
            }
        }
        return frames;
    };
    // convert frames to time elapsed
    TimeConverter.prototype.toTime = function (frame) {
        var FRAME_REGEX_EXP = '^[-]?[0-9]+$';
        var tokens = frame.match(FRAME_REGEX_EXP);
        // sanity check on input tokens
        /* istanbul ignore if */
        if (tokens == null) {
            console.log("error: invalid frame (input: ".concat(frame, ") passed into 'toTime' within TimeConverter"));
            return undefined;
        }
        // tokens is structured as follows
        // [ <frame>, <frame>, index: <starting index>, input: <frame>, groups: undefined ]
        var n_frame = parseInt(frame);
        // check if negative and update value if needed
        var is_negative = n_frame < 0;
        if (is_negative) {
            n_frame *= -1;
        }
        // sanity check that we are operating on non-negative numbers
        if (n_frame < 0) {
            console.log("error: frame somehow became negative after normalization");
            return undefined;
        }
        // 60 frames/second
        var total_seconds = Math.floor(n_frame / 60);
        var minutes = Math.floor(total_seconds / 60);
        var seconds = total_seconds % 60;
        // format the time nicely
        /* istanbul ignore next */
        var time = (is_negative ? "-" : "") +
            (minutes < 10 ? "0".concat(minutes, ":") : "".concat(minutes, ":")) +
            (seconds < 10 ? "0".concat(seconds) : "".concat(seconds));
        // do we do sanity game checks
        /* istanbul ignore if */
        if (this.gameCheck) {
            // yes we do the checks
            // check and make sure we have a game to check
            if (this.game == null) {
                console.log("error: supplied game is null");
                return undefined;
            }
            // get the frames
            var gameFrames = this.game.getFrames();
            /* istanbul ignore if */
            if (gameFrames == undefined) {
                console.log("supplied game has undefined frames");
                return undefined;
            }
            // get the frame in question and check it
            var frameInQuestion = gameFrames[n_frame];
            if (frameInQuestion == null || frameInQuestion == undefined) {
                return undefined;
            }
        }
        return time;
    };
    return TimeConverter;
}());
export { TimeConverter };
//# sourceMappingURL=TimeConverter.js.map